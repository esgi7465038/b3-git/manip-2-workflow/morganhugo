<?php 
    namespace Entities;

    class Livre {
        private $titre;
        private $editeur;
        private $isbn;
    
        public function __construct($titre, $editeur, $isbn) {
            $this->titre = $titre;
            $this->editeur = $editeur;
            $this->isbn = $isbn;
        }
    
        public function getTitre() {
            return $this->titre;
        }
    
        public function getEditeur() {
            return $this->editeur;
        }
    
        public function getISBN() {
            return $this->isbn;
        }
    
        private function setTitre($titre) {
            if (strlen($titre) <= 50) {
                $this->titre = $titre;
            } else {
                throw new InvalidArgumentException("Le titre doit avoir au maximum 50 caractères.");
            }
        }
    
        private function setEditeur($editeur) {
            if (strlen($editeur) <= 25) {
                $this->editeur = $editeur;
            } else {
                throw new InvalidArgumentException("L'éditeur doit avoir au maximum 25 caractères.");
            }
        }
    
        private function setISBN($isbn) {
            if (preg_match('/^\d{13}$/', $isbn)) {
                $this->isbn = $isbn;
            } else {
                throw new InvalidArgumentException("L'ISBN doit contenir exactement 13 caractères numériques.");
            }
        }
    }


    
    
    
    