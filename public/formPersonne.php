<?php
    session_start();
    
    include_once '../config/appConfig.php';
    use Entities\Personne;
?>

<!DOCTYPE html>
<html>
<head>
    <title>Formulaire de Création de Personne</title>
    <link rel="stylesheet" href="css/main.css">
    <?php include_once 'inc/head.php'; ?>
</head>
<body>
	<?php include_once 'inc/header.php'; ?>

    <div class="pageForm">
        <h1>Formulaire de Création de Personne</h1>
        <form action="formPersonne.php" method="post">
            <label for="nom">Nom :</label>
            <input type="text" id="nom" name="nom" maxlength="50" required><br>

            <label for="prenom">Prénom :</label>
            <input type="text" id="prenom" name="prenom" maxlength="25" required><br>

            <label for="date_permis">Date du permis :</label>
            <input type="date" id="date_permis" name="date_permis" title="La date du permis est obligatoire" required><br>

            <input type="submit" value="Créer une Personne">
        </form>
    </div>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Récupérez les données POST
        $nom = $_POST["nom"];
        $prenom = $_POST["prenom"];
        $date_permis = $_POST["date_permis"];
      
        // Créez un tableau pour stocker les personnes en session s'il n'existe pas déjà
        if (!isset($_SESSION["personnes"])) {
            $_SESSION["personnes"] = array();
        }
    
        if ($nom && $prenom && $date_permis) {
            $personne = new Personne($nom, $prenom, $date_permis);
            $_SESSION["personnes"][] = $personne; // Ajoutez le personne au tableau en session

            echo "La personne à bien été créé";

        } else {
            echo "Aucune personne n'a été ajouté en session.";
        }
        exit();
      }
      ?>

    <?php include_once 'inc/footer.php'; ?>
</body>
</html>