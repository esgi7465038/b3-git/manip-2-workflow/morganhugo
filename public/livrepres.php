<?php
/**
 * ATTENTION!! 
 * Les deux lignes PHP suivantes doivent être incluses dans toutes vos pages "exécutable"
 */

//  Permet d'utiliser le typage fort si strict_types=1
//  ATTENTION!! Laisser en première ligne de toutes vos pages
declare(strict_types=1);

require_once '../config/appConfig.php';
use Entities\Livre;
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>Présentation des livres</title>
	<?php include_once 'inc/head.php'; ?>
    </head>
    <body>
	<?php include_once 'inc/header.php'; ?>

        <main>
	    	<article class="container mx-auto">
				<header class="">
				    <h1 class="text-4xl font-bold">Bienvenue dans la présentaion de nos livres</h1>
				</header>
				<div class="livres">
					<?php
						if (isset($_SESSION["livres"]) && !empty($_SESSION["livres"])) {
							foreach ($_SESSION["livres"] as $index => $livre) {
								echo "<div class='livre'>";
								echo "<p><strong>Livre " . ($index + 1) . " :</strong><br>";
								echo "Titre : " . $livre->getTitre() . "<br>";
								echo "Editeur : " . $livre->getEditeur() . "<br>";
								echo "ISBN : " . $livre->getISBN() . "</p>";
								echo "<br></div>";
							}
						} else {
							echo "<p>Aucun livre en session.</p>";
						}
					?>
				</div>
	    	</article>
        </main>

	<?php include_once 'inc/footer.php'; ?>
    </body>
</html>

