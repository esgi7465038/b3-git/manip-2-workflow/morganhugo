<?php 
    namespace Entities;

    class Personne {
        private $nom;
        private $prenom;
        private $date_permis;
    
        public function __construct($nom, $prenom, $date_permis) {
            $this->setNom($nom);
            $this->setPrenom($prenom);
            $this->setDate($date_permis);
        }
    
        public function getNom() {
            return $this->nom;
        }
    
        public function getPrenom() {
            return $this->prenom;
        }
    
        public function getDate() {
            return $this->date_permis;
        }
    
        private function setNom($nom) {
            if (strlen($nom) <= 50) {
                $this->nom = $nom;
            } else {
                throw new InvalidArgumentException("Le nom doit avoir au maximum 50 caractères.");
            }
        }
    
        private function setPrenom($prenom) {
            if (strlen($prenom) <= 25) {
                $this->prenom = $prenom;
            } else {
                throw new InvalidArgumentException("Le Prénom doit avoir au maximum 25 caractères.");
            }
        }
    
        private function setDate($date_permis) {
            if (date($date_permis)) {
                $this->date_permis = $date_permis;
            } else {
                throw new InvalidArgumentException("La date doit être numérique.");
            }
        }
        
    }


    
    
    
    