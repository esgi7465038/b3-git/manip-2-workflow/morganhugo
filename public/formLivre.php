<?php
    require_once '../config/appConfig.php';
    //require_once '../src/Entities/Livre.php';
    use Entities\Livre;
?>

<!DOCTYPE html>
<html>
<head>
    <title>Formulaire de Création de Livre</title>
    <?php include_once 'inc/head.php'; ?>
</head>
<body>
	<?php include_once 'inc/header.php'; ?>
    <div class="pageForm">
        <h1>Formulaire de Création de Livre</h1>
        <form action="formLivre.php" method="post">
            <label for="titre">Titre :</label>
            <input type="text" id="titre" name="titre" maxlength="50" required><br>

            <label for="editeur">Éditeur :</label>
            <input type="text" id="editeur" name="editeur" maxlength="25" required><br>

            <label for="isbn">ISBN :</label>
            <input type="text" id="isbn" name="isbn" pattern="\d{13}" title="L'ISBN doit contenir 13 chiffres" required><br>

            <input type="submit" value="Créer Livre">
        </form>
    </div>
    
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Récupérez les données POST
        $titre = $_POST["titre"];
        $editeur = $_POST["editeur"];
        $isbn = $_POST["isbn"];
    
        // Créez un tableau pour stocker les livres en session s'il n'existe pas déjà
        if (!isset($_SESSION["livres"])) {
            $_SESSION["livres"] = array();
        }
    
        if ($titre && $editeur && $isbn) {
            $livre = new Livre($titre, $editeur, $isbn);
            $_SESSION["livres"][] = $livre; // Ajoutez le livre au tableau en session

            echo "Le livre à bien été créer";

        } else {
            echo "Aucun livre n'a été ajouté en session.";
        }
        exit();
    }
    ?>

	<?php include_once 'inc/footer.php'; ?>
</body>
</html>