<?php 
    include_once '../config/appConfig.php';
    use Entities\Livre;

    try {
        // Créez une instance de la classe Livre en lui passant les valeurs initiales
        $livre = new Livre("Titre du livre", "Éditeur ABC", "1234567890123");
    
        // Accédez aux attributs et affichez-les
        echo "Titre : " . $livre->getTitre() . "<br>";
        echo "Éditeur : " . $livre->getEditeur() . "<br>";
        echo "ISBN : " . $livre->getISBN() . "<br>";
    
    } catch (InvalidArgumentException $e) {
        // Attrapez les exceptions si une vérification échoue
        echo "Erreur : " . $e->getMessage();
    }