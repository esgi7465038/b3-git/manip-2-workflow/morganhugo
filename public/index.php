<?php
/**
 * ATTENTION!! 
 * Les deux lignes PHP suivantes doivent être incluses dans toutes vos pages "exécutable"
 */

//  Permet d'utiliser le typage fort si strict_types=1
//  ATTENTION!! Laisser en première ligne de toutes vos pages
declare(strict_types=1);

require_once '../config/appConfig.php';
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>Projet</title>
	<?php include_once 'inc/head.php'; ?>
    </head>
    <body>
	<?php include_once 'inc/header.php'; ?>

        <main>
	    	<article class="container mx-auto">
				<header class="my-10">
				    <h1 class="text-4xl font-bold">Bienvenue sur notre site web</h1>
				</header>
				<p class="text-center w-8/12 mx-auto">Bienvenue sur notre site web dédié à la
					 démonstration de l'architecture des fichiers 
					 d'une application web. Nous sommes ravis de vous
					  accueillir et de vous offrir un aperçu complet de 
					  la manière dont les fichiers sont organisés au sein 
					  d'une application web moderne.
				</p>
				<div class="my-10">
					<h2 class="text-2xl font-bold text-center mb-5">Explorez l'Organisation des Fichiers</h2>
					<p class="w-8/12 mx-auto">
						Notre site vous propose une exploration détaillée de l'organisation des fichiers au sein d'une application web. Nous avons structuré notre contenu de manière à ce que vous puissiez facilement comprendre les différents éléments qui composent une application web, tels que les fichiers HTML, CSS, JavaScript, et bien d'autres encore.
					</p>
				</div>
				<div class="my-10">
					<h2 class="text-2xl font-bold text-center mb-5">Consultez le Fichier README.MD</h2>
					<p class="w-8/12 mx-auto">
						Pour obtenir des informations approfondies sur l'architecture des fichiers d'une application web, nous vous recommandons de consulter notre fichier README.MD. Ce fichier constitue une ressource précieuse contenant des instructions, des descriptions et des explications qui vous guideront à travers chaque aspect de l'organisation des fichiers.
					</p>
				</div>
				<div class="mt-10 mx-auto flex gap-10 justify-center items-center">
					<div>
						<a href="formLivre.php" class="hover:text-blue-500 transition-all">Ajouter un livre</a>
					</div>
					<div>
						<a href="livrepres.php" class="hover:text-blue-500 transition-all">Voir les livres</a>
					</div>
					<div>
						<a href="deleteLivre.php" class="hover:text-blue-500 transition-all">Supprimer un livre</a>
					</div>
					<div>
						<a href="formPersonne.php" class="hover:text-blue-500 transition-all">Ajouter une personne</a>
					</div>
					<div>
						<a href="presPersonne.php" class="hover:text-blue-500 transition-all">Voir les personnes</a>
					</div>
					<div>
						<a href="deletePersonne.php" class="hover:text-blue-500 transition-all">Supprimer les personnes</a>
					</div>
				<div>
	    	</article>
        </main>

	<?php include_once 'inc/footer.php'; ?>
    </body>
</html>

