<?php
/**
 * ATTENTION!! 
 * Les deux lignes PHP suivantes doivent être incluses dans toutes vos pages "exécutable"
 */

//  Permet d'utiliser le typage fort si strict_types=1
//  ATTENTION!! Laisser en première ligne de toutes vos pages
declare(strict_types=1);

require_once '../config/appConfig.php';
use Entities\Livre;
?>
<!DOCTYPE html>
<html>
<head>
    <title>Supprimer un livre</title>
    <?php include_once 'inc/head.php'; ?>
</head>
<body>
	<?php include_once 'inc/header.php'; ?>

    <div class="pageForm">
        <h1>Supprimer un livre</h1>
        <form method="post" action="deleteLivre.php">
            <select id="livres" name="livre_a_supprimer">
                <?php 
                    foreach ($_SESSION["livres"] as $livre) {
                        echo "<option value='" . $livre->getTitre() . "'>" . $livre->getTitre() . "</option>";
                    }
                ?>
            </select>
            <input type="submit" class="boutonDelete" value="Supprimer le livre">
        </form>
    </div>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_POST["livre_a_supprimer"])) {
            $livre_a_supprimer = $_POST["livre_a_supprimer"];

            // Parcourir le tableau $_SESSION["livres"] pour trouver l'objet à supprimer
            foreach ($_SESSION["livres"] as $index => $livre) {
                if ($livre->getTitre() == $livre_a_supprimer) {
                    // Supprimer l'objet du tableau
                    unset($_SESSION["livres"][$index]);
                    break; // Sortir de la boucle une fois que l'objet est supprimé
                }
            }
        }
    }
    ?>
    <?php include_once 'inc/footer.php'; ?>
</body>
</html>




