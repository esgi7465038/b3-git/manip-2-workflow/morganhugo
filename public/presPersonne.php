<?php
/**
 * ATTENTION!! 
 * Les deux lignes PHP suivantes doivent être incluses dans toutes vos pages "exécutable"
 */

//  Permet d'utiliser le typage fort si strict_types=1
//  ATTENTION!! Laisser en première ligne de toutes vos pages
declare(strict_types=1);

require_once '../config/appConfig.php';
use Entities\Livre;
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>Présentation des personnes</title>
	<?php include_once 'inc/head.php'; ?>
    </head>
    <body>
	<?php include_once 'inc/header.php'; ?>

        <main>
	    	<article class="container mx-auto">
				<header class="">
				    <h1 class="text-4xl font-bold">Bienvenue dans la présentaion de nos personnes</h1>
				</header>
				<div class="livres">
					<?php
						if (isset($_SESSION["personnes"]) && !empty($_SESSION["personnes"])) {
							foreach ($_SESSION["personnes"] as $index => $personne) {
								echo "<div class='livre'>";
								echo "<p><strong>Personne " . ($index + 1) . " :</strong><br>";
								echo "Nom : " . $personne->getNom() . "<br>";
								echo "Prénom : " . $personne->getPrenom() . "<br>";
								echo "Date du Permis : " . $personne->getDate() . "</p>";
								echo "<br></div>";
							}
						} else {
							echo "<p>Aucune personne en session.</p>";
						}
					?>
				</div>
	    	</article>
        </main>

	<?php include_once 'inc/footer.php'; ?>
    </body>
</html>

