<?php
/**
 * ATTENTION!! 
 * Les deux lignes PHP suivantes doivent être incluses dans toutes vos pages "exécutable"
 */

//  Permet d'utiliser le typage fort si strict_types=1
//  ATTENTION!! Laisser en première ligne de toutes vos pages
declare(strict_types=1);

require_once '../config/appConfig.php';
use Entities\Personne;
?>
<!DOCTYPE html>
<html>
<head>
    <title>Supprimer une personne</title>
    <?php include_once 'inc/head.php'; ?>
</head>
<body>
	<?php include_once 'inc/header.php'; ?>
    <h1>Supprimer une personne</h1>

    <form method="post" action="deletepersonne.php">
        <select id="personnes" name="personne_a_supprimer">
            <?php 
                foreach ($_SESSION["personnes"] as $personne) {
                    echo "<option value='" . $personne->getNom() . "'>" . $personne->getNom() . "</option>";
                }
            ?>
        </select>
        <input type="submit" value="Supprimer la personne">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_POST["personne_a_supprimer"])) {
            $personne_a_supprimer = $_POST["personne_a_supprimer"];

            // Parcourir le tableau $_SESSION["personnes"] pour trouver l'objet à supprimer
            foreach ($_SESSION["personnes"] as $index => $personne) {
                if ($personne->getNom() == $personne_a_supprimer) {
                    // Supprimer l'objet du tableau
                    unset($_SESSION["personnes"][$index]);
                    break; // Sortir de la boucle une fois que l'objet est supprimé
                }
            }
        }
    }
    ?>
    <?php include_once 'inc/footer.php'; ?>
</body>
</html>




